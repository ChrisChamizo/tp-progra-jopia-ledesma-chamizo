package juego;

import java.awt.Color;

import entorno.Entorno;

public class Calle {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Color color;
	private double velocidad;
		// Constructor
	public Calle(double x, double y, double ancho, double alto, Color color, double velocidad) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.color = color;
		this.velocidad = velocidad;

	}

	public void mover() {
		y = y + velocidad;

	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(x, y, ancho, alto, 0, color);
	}

	public void reaparecer(Entorno entorno) {
		if (y -( alto/2) > entorno.alto()) {
			y = 0 - alto/2 ;
		}

	}

}


