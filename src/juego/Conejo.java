package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Conejo {
	private int x;
	private double y;
	private int alto;
	private int ancho;
	private Image conejo;
	

	Conejo(int x, int y, int alto, int ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.conejo=Herramientas.cargarImagen("Conejo.png");
	}

	public void saltoDerecha(Entorno entorno) {
		if (x + 80  < entorno.ancho()-10) {
			this.x += 80;
		}else { x = entorno.ancho()-10;
		 }
	}

	public void saltoIzquierda() {
		if (x - 80 > 10) {
			   x -= 80;
		}else {
			x = 10;
		}

	}

	public void dibujar(Entorno entorno) {
		
		entorno.dibujarImagen(conejo, x, y, 0, 0.4);
	}

	public void caer() {

		this.y = this.y + 0.2;

	}

	public void subir() {
		if (y - 35 > 0) {
			this.y -= 35;
		}else { y = 10;	
		}
	}

	public int getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

	public int getAncho() {
		return ancho;
	}

	public Kamehameha lanzarKamehameha() {
//		
		return new Kamehameha(this.x, this.y);
	}

	public boolean colisionConejoAuto(Vehiculo[][] a) {

		for (int i = 0; i < a.length; i++) {
			for (int e = 0; e < a[i].length; e++) {
				if (a[i][e] != null && x - ancho / 2 < a[i][e].getX() + a[i][e].getAncho() / 2 &&

						a[i][e].getX() - a[i][e].getAncho() / 2 < x + ancho / 2 &&

						(y - alto / 2) < a[i][e].getY() + (a[i][e].getAlto() / 2) &&

						a[i][e].getY() - a[i][e].getAlto() < y + alto / 2) {

					return true;
				}
			}
		}
		return false;

	}

}
