package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	private Entorno entorno;
	private Conejo conejo;
	private int vidas;
	private Calle calle2;
	private Calle calle1;
	private Calle calle3;
	private int saltos;
	private int puntos;
	private Vehiculo[][] auto1;
	private Vehiculo[][] auto2;
	private Vehiculo[][] auto3;
	private Kamehameha kamehameha;
	private Image ganaste;
	private Image gameOver;
	private Image fondo;
	public Juego() {
		this.entorno = new Entorno(this, "Jopia - Ledesma - Chamizo", 800, 600);
		this.conejo = new Conejo(this.entorno.ancho() / 2, 510, 20, 20);
		this.vidas = 3;
		this.saltos = 0;
		this.puntos = 0;
		ganaste = Herramientas.cargarImagen("Ganaste.jpg");
		gameOver = Herramientas.cargarImagen("gameOver.jpg");
		fondo=Herramientas.cargarImagen("fondo.jpg");
		this.entorno.iniciar();
		this.calle1 = new Calle(entorno.ancho() / 2, 107, entorno.ancho(), 140, Color.GRAY, 0.2);
		this.calle2 = new Calle(entorno.ancho() / 2, 354, entorno.ancho(), 140, Color.GREEN, 0.2);
		this.calle3 = new Calle(entorno.ancho() / 2, -138, entorno.ancho(), 140, Color.WHITE, 0.2);
		this.auto1 =  Vehiculo.crearMano2(auto1, 301);
		this.auto2 =  Vehiculo.crearMano2(auto2, 55);
		this.auto3 =  Vehiculo.crearMano2(auto3,-190);
		
		}
	public void tick() {
		if (!juegoTerminado() && !gano()) {
			fondo();
			accionesConejo();calle1.dibujar(entorno);
			calle2.dibujar(entorno);
			calle3.dibujar(entorno);
			calle1.mover();
			calle2.mover();
			calle3.mover();
			calle1.reaparecer(entorno);
			calle2.reaparecer(entorno);
			calle3.reaparecer(entorno);

			Vehiculo.recorrido(auto1, entorno);
			Vehiculo.recorrido(auto2, entorno);
			Vehiculo.recorrido(auto3, entorno);
			conejo.dibujar(this.entorno);
			if (this.conejo.getY() <= entorno.alto()) {
				conejo.caer();
			} else {
				reiniciarJuego();
			}
			if (kamehameha != null && kamehameha.getY() <= 0) {
				kamehameha = null;
			}
			if (kamehameha != null && kamehameha.colisionKameAuto(auto1)
					|| kamehameha != null && kamehameha.colisionKameAuto(auto2)
					|| kamehameha != null && kamehameha.colisionKameAuto(auto3)) {
				kamehameha = null;
				puntos += 5;
			}
			if (kamehameha != null) {
				kamehameha.dibujar(entorno);
				kamehameha.mover();

			}

		
			  if (Vehiculo.seFuePantalla(auto1, entorno)) {
				  this.auto1 =  Vehiculo.crearMano2(auto1,-78);
			  }
			  			  
			  if (Vehiculo.seFuePantalla(auto2, entorno)) {
				  this.auto2 =  Vehiculo.crearMano2(auto2, -78);
			  }
 				if (Vehiculo.seFuePantalla(auto3, entorno)) {
 					this.auto3 =  Vehiculo.crearMano2(auto3,-78);
 				}		 
			if (conejo.colisionConejoAuto(auto1)) {
				reiniciarJuego();
				
			}
			if (conejo.colisionConejoAuto(auto2)) {
				reiniciarJuego();
				
			}
			if (conejo.colisionConejoAuto(auto3)) {
				reiniciarJuego();
				
			}
			

			mostrarPuntos();
			mostrarVidas();
			mostrarSaltos();
		}
		if (juegoTerminado()) {
			fondo();
			mostrarPuntos();
			mostrarVidas();
			mostrarSaltos();
			entorno.dibujarImagen(gameOver, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);
		}
		if (gano()) {
			fondo();
			mostrarPuntos();
			mostrarVidas();
			mostrarSaltos();
			entorno.dibujarImagen(ganaste, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);

		}
	}

	public void accionesConejo() {
		if (this.entorno.sePresiono(this.entorno.TECLA_DERECHA)) {
			this.conejo.saltoDerecha(entorno);

		}
		if (this.entorno.sePresiono(this.entorno.TECLA_IZQUIERDA)) {
			this.conejo.saltoIzquierda();
		}
		if (this.entorno.sePresiono(this.entorno.TECLA_ARRIBA)) {
			this.conejo.subir();
			this.saltos++;
		}

		if (kamehameha == null && this.entorno.sePresiono(this.entorno.TECLA_ESPACIO)) {
			kamehameha = conejo.lanzarKamehameha();
		}

	}

	public void reiniciarJuego() {
									
							
		this.vidas -= 1;
		this.conejo = new Conejo(this.entorno.ancho() / 2, 510, 20, 20);
		this.saltos = 0;
		this.calle1 = new Calle(entorno.ancho() / 2, 107, entorno.ancho(), 140, Color.GRAY, 0.2);
		this.calle2 = new Calle(entorno.ancho() / 2, 354, entorno.ancho(), 140, Color.GREEN, 0.2);
		this.calle3 = new Calle(entorno.ancho() / 2, -138, entorno.ancho(), 140, Color.WHITE, 0.2);
		this.auto1 =  Vehiculo.crearMano2(auto1,  301);
		this.auto2 =  Vehiculo.crearMano2(auto2, 55);
		this.auto3 =  Vehiculo.crearMano2(auto3,-190);
	   }				
		
	public void mostrarVidas() {
		String cantVidas = "Vidas: " + this.vidas;
		this.entorno.cambiarFont("Calibri", 25, Color.blue);
		this.entorno.escribirTexto(cantVidas, 10, 30);
	}

	public void mostrarSaltos() {
		String saltos = "saltos: " + this.saltos;
		this.entorno.cambiarFont("Calibri", 25, Color.blue);
		this.entorno.escribirTexto(saltos, 140, 30);
	}

	public void mostrarPuntos() {
		String puntos = "puntos: " + this.puntos;
		this.entorno.cambiarFont("Calibri", 25, Color.blue);
		this.entorno.escribirTexto(puntos, 270, 30);
	}

	
	

	public boolean juegoTerminado() {
		if (this.vidas == 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean gano() {
		if (this.puntos == 50) {
			return true;
		}
		return false;
	}

public void fondo() {
	this.entorno.dibujarImagen(fondo,420, 270, 0, 3);
}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
