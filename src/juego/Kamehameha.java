package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Kamehameha {
	private int x;
	private double y;
	private double ancho;
	private int alto;
	private Image kameha;
	
	public Kamehameha(int x, double y) {
		
		this.x = x;
		this.y = y;
		this.alto=30;
		this.ancho=30;
		kameha=Herramientas.cargarImagen("kameha.png");
	}
	
	public void mover() {
		this.y-=10;
	}
	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.yellow);
		entorno.dibujarImagen(kameha, x, y, 0, 1);
		
	}
	public int getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public int getAlto() {
		return alto;
	}

	
			public boolean colisionKameAuto(Vehiculo[][] a) {

			  for (int i = 0; i < a.length; i++) {
			   for (int e = 0; e < a[i].length; e++) {

			    if (a[i][e] != null && x - ancho / 2 < a[i][e].getX() + a[i][e].getAncho() / 2 

			       && a[i][e].getX() - a[i][e].getAncho() / 2 < x + ancho / 2 &&

			      (y - alto / 2) < a[i][e].getY() + (a[i][e].getAlto() / 2) &&

			      a[i][e].getY() - a[i][e].getAlto() < y + alto / 2) {
			     a[i][e] = null;
			     return true;

			    }

			   }
			  }

			  return false;
			 }
}
