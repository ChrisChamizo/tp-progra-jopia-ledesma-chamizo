package juego;

import java.awt.Color;

import entorno.Entorno;

public class Vehiculo {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Color color;
	private double velocidad;
	

	public Vehiculo(double x, double y, double alto, double ancho, Color color, double velocidad) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.color = color;
		this.velocidad = velocidad;

	}

	public void mover() {
		y = y + 0.2;

	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getAlto() {
		return this.alto;
	}

	public double getAncho() {
		return this.ancho;
	}

	public Color getColor() {
		return this.color;
	}

	public double getVelocidad() {
		return this.velocidad;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, this.color);
	}

	public void moverDerecha() {
		this.x += this.velocidad;
	}

	public void moverIzquierda() {
		this.x -= this.velocidad;
	}

	public void reaparecerAuto() {
		if (this.x >= 800) {
			this.x = 0;

		} else if (x <= 0) {
			this.x = 800;
		}
	}

	public static void recorrido(Vehiculo[][] auto, Entorno entorno) {
		for (int i = 0; i < auto.length; i++) {
			for (int e = 0; e < auto[i].length; e++) {
				if (auto[i][e] != null) {
					auto[i][e].mover();
					auto[i][e].dibujar(entorno);
					auto[i][e].reaparecerAuto();
				}

				if (i % 2 == 0) {
					if (auto[i][e] != null) {
						auto[i][e].moverDerecha();
					}
				} else {
					if (auto[i][e] != null) {
						auto[i][e].moverIzquierda();
					}
				}
			}
		}
	}

	public static boolean seFuePantalla(Vehiculo[][] autos, Entorno entorno) {
		for (int e = 0; e < autos[0].length; e++) {
			if (autos[0][e] != null && autos[0][e].y - autos[0][e].ancho - 25 / 2 > entorno.alto()) {

				return true;
			}
		}
		return false;
	}

	public static Vehiculo[] mano(Vehiculo a, Vehiculo b, Vehiculo c) {
		Vehiculo[] vehiculos = { a, b, c };
		return vehiculos;
	}
	
	public static Vehiculo[][] crearMano2(Vehiculo[][] autos, int y) {
		 autos = new Vehiculo[4][3];
		 for (int i = 0; i < autos.length; i++) {
				autos[i]=mano(new Vehiculo(0, y+35*i, 20, 50, Color.red, 0.5+0.5*i), new Vehiculo(500, y+35*i, 20, 50, Color.red, 0.5+0.5*i),
						new Vehiculo(200,y+35*i, 20, 50, Color.red, 0.5+0.5*i));
					
					
				}
		 
		 return autos;
	}
}
	
			
